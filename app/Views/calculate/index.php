<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Developer Technical Test</title>
    <link rel="stylesheet" href="<?= PROJECT_ROOT ?>css/bootstrap.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="<?= PROJECT_ROOT ?>css/custom.css" media="screen" charset="utf-8">
    <script src="<?= PROJECT_ROOT ?>js/jquery-3.4.1.min.js"></script>
    <script src="<?= PROJECT_ROOT ?>js/bootstrap.min.js"></script>
    <script src="<?= PROJECT_ROOT ?>js/jquery.validate.min.js"></script>
    <script src="<?= PROJECT_ROOT ?>js/calculate-form-validate.js"></script>
</head>
<body class="bg-light">

<div class="container">
    <div class="py-5 text-center">
        <h2>Developer Technical Test</h2>
    </div>

    <div class="row">
        <div class="col-md-6 order-md-2 mb-6">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Result</span>
                <span class="badge badge-secondary badge-pill badge-result">0</span>
            </h4>
            <table class="table table-striped table-sm table-result">
                <thead>
                <tr>
                    <th>Month Year</th>
                    <th>Number Studies</th>
                    <th>Cost forecasted</th>
                </tr>
                </thead>
                <tbody>
                <tr class="no-data">
                    <td colspan="3">No data</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 order-md-1">
            <h4 class="mb-3">Input Parameters</h4>
            <form class="calculate-form">
                <div class="row">
                    <div class="form-group col-md-12 mb-3">
                        <label>Current Number of study per day</label>
                        <input name="studies" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-12 mb-3">
                        <label>Number of Study Growth per month</label>
                        <input name="growth" type="text" class="form-control">
                        <small class="text-muted">Value from 0 to 100</small>
                    </div>
                    <div class="form-group col-md-12 mb-3">
                        <label>Number of months to forecast</label>
                        <input name="months" type="text" class="form-control">
                        <small class="text-muted">Value from 1 to 12</small>
                    </div>
                </div>
                <hr class="mb-4">
                <button data-url="<?= PROJECT_ROOT ?>calculate/submit" class="btn btn-dark btn-lg btn-block"
                        type="submit">
                    Calculate
                </button>
            </form>
        </div>
    </div>
</div>

</body>
</html>