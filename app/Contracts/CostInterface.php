<?php

namespace App\Contracts;

/**
 * Interface CostInterface
 * @package App\Contracts
 */
interface CostInterface
{
    /**
     * @return mixed
     */
    public function getCost();
}