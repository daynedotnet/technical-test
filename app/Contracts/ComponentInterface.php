<?php

namespace App\Contracts;

/**
 * Interface ComponentInterface
 * @package App\Contracts
 */
interface ComponentInterface
{
    /**
     * @param array $parameters
     * @return mixed
     */
    public function getTotal(array $parameters);
}