<?php

namespace App\Services;

use App\Contracts\ComponentInterface;

/**
 * Class StorageService
 * @package App\Services
 */
class StorageService implements ComponentInterface
{
    /**
     * @param array $parameters
     * @return float|int
     */
    public function getTotal(array $parameters)
    {
        // TODO: Implement getTotal() method.

        $consumption = $parameters['studies'] * 0.01;

        return ($consumption * STORAGE_PER_MONTH);
    }
}