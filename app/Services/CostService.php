<?php

namespace App\Services;

use App\Contracts\CostInterface;
use App\Factories\ComponentFactory;

/**
 * Class CostService
 * @package App\Services
 */
class CostService implements CostInterface
{
    /**
     * @var
     */
    protected $parameters;
    /**
     * @var ComponentFactory
     */
    protected $componentFactory;

    /**
     * CostService constructor.
     * @param $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
        $this->componentFactory = new ComponentFactory();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCost()
    {
        // TODO: Implement getTotal() method.

        $startPeriod = new \DateTime(CURRENT_MONTH);
        $endPeriod = new \DateTime(self::addMonth($this->parameters['months']));
        $interval = \DateInterval::createFromDateString("1 month");

        $period = new \DatePeriod($startPeriod, $interval, $endPeriod);
        $cost = array();

        // loop through months based on current month
        foreach ($period as $index => $dt) {
            $periodMonth = $dt->format("m");
            $periodMonthName = $dt->format("M");
            $periodYear = $dt->format("Y");

            // if not first element
            if ($index) {
                $growth = $this->parameters['growth'] / 100;
                $this->parameters['studies'] += ($this->parameters['studies'] * $growth);
            }

            // get which component to manufacture and instantiate
            $ram = $this->componentFactory->create('ram');
            $storage = $this->componentFactory->create('storage');

            // get each total based on components
            $args = ['studies' => $this->parameters['studies'], 'days' => self::calculateDaysInMonth($periodYear, $periodMonth)];
            $ramTotal = $ram->getTotal($args);
            $storageTotal = $storage->getTotal($args);

            // get total cost
            $costArray = [$ramTotal, $storageTotal];
            $totalCost = array_sum($costArray);

            // make array of cost
            $cost[] = array(
                'month_year' => "$periodMonthName $periodYear",
                'number_studies' => number_format(floor($this->parameters['studies'])),
                'cost_forecasted' => "$" . number_format($totalCost, 2)
            );
        }

        return $cost;
    }

    /**
     * @param $value
     * @return false|string
     */
    protected function addMonth($value)
    {
        return date('Y-m', strtotime("+$value months", strtotime(CURRENT_MONTH)));
    }

    /**
     * @param $year
     * @param $month
     * @return int
     */
    protected function calculateDaysInMonth($year, $month)
    {
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }
}