<?php

namespace App\Services;

use App\Contracts\ComponentInterface;

/**
 * Class RamService
 * @package App\Services
 */
class RamService implements ComponentInterface
{
    /**
     * @param array $parameters
     * @return float|int
     */
    public function getTotal(array $parameters)
    {
        // TODO: Implement getTotal() method.

        $consumption = ($parameters['studies'] * 0.5) / 1000;

        return ($consumption * RAM_PER_HOUR) * ($parameters['days'] * 24);
    }
}