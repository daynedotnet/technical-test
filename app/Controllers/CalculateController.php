<?php

namespace App\Controllers;

use App\Factories\CostFactory;
use Core\Controller;

/**
 * Class CalculateController
 * @package App\Controllers
 */
class CalculateController extends Controller
{
    /**
     * @var CostFactory
     */
    protected $costFactory;

    /**
     * CalculateController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->costFactory = new CostFactory();
    }

    public function index()
    {
        // render calculate/index view
        $this->view->render('calculate/index');
    }

    /**
     * @return int|void
     * @throws \Exception
     */
    public function submit()
    {
        // if http request is not post method return 403
        if (!$this->request->isPost())
            return http_response_code(403);

        $parameters = $_POST;

        $usage = $this->costFactory->create($parameters);

        $response = $usage->getCost();

        return $this->json($response);
    }
}