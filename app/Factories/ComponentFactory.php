<?php

namespace App\Factories;

use App\Services\RamService;
use App\Services\StorageService;

/**
 * Class ComponentFactory
 * @package App\Factories
 */
class ComponentFactory
{
    /**
     * @param $component
     * @return RamService|StorageService
     */
    public static function create($component)
    {
        switch ($component) {
            case 'ram' :
                return new RamService();
                break;

            case 'storage' :
                return new StorageService();
                break;
        }
    }
}