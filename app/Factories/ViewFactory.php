<?php

namespace App\Factories;

use Core\View;

/**
 * Class ViewFactory
 * @package App\Factories
 */
class ViewFactory
{
    /**
     * @return View
     */
    public static function create()
    {
        return new View();
    }
}