<?php

namespace App\Factories;

use Core\Request;

/**
 * Class RequestFactory
 * @package App\Factories
 */
class RequestFactory
{
    /**
     * @return Request
     */
    public static function create()
    {
        return new Request();
    }
}