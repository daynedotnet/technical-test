<?php

namespace App\Factories;

use App\Services\CostService;

/**
 * Class CostFactory
 * @package App\Factories
 */
class CostFactory
{
    /**
     * @param $parameters
     * @return CostService
     */
    public static function create($parameters)
    {
        return new CostService($parameters);
    }
}