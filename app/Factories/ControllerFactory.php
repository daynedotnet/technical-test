<?php

namespace App\Factories;

/**
 * Class ControllerFactory
 * @package App\Factories
 */
class ControllerFactory
{
    /**
     * @param $controller
     * @return mixed
     */
    public static function create($controller)
    {
        return new $controller();
    }
}