## Developer Technical Test

Forecast the cost of infrastructure on customer usage and expected growth.

## Get your local Git repository on Bitbucket

Step 1: Switch to your local directory

```bash
cd /path/to/your/local
```

Step 2: Clone existing repository to yout local

```bash
git clone https://daynedotnet@bitbucket.org/daynedotnet/technical-test.git
```

## Change Project Root PATH

```bash
cd config
```

```bash
vi config.php
```

Change PROJECT_ROOT constant depends on your environment setup

If you chose technical-test as your project name

Visit http://localhost/technical-test

## Contributing
Written by Ralph Dayne B. Banzon using PHP with OOP and Factory Pattern

## License
[MIT](https://opensource.org/licenses/MIT)