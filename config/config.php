<?php

setlocale(LC_MONETARY, 'en_US');
define('DEFAULT_CONTROLLER', 'CalculateController'); // default controller if there isn't one defined in the url
define('PROJECT_ROOT', '/technical-test/'); // set this to '/' on production server.
define('RAM_PER_HOUR', 0.00553); // The cost of 1GB of RAM per hour
define('STORAGE_PER_MONTH', 0.1); // 1GB of storage cost per month
define('CURRENT_MONTH', date('Y-m'));