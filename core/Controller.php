<?php

namespace Core;

use App\Factories\ComponentFactory;
use App\Factories\RequestFactory;
use App\Factories\ViewFactory;

/**
 * Class Controller
 * @package Core
 */
class Controller
{
    /**
     * @var View
     */
    /**
     * @var Request|View
     */
    public $view, $request;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->request = RequestFactory::create();
        $this->view = ViewFactory::create();
    }

    /**
     * @param $response
     */
    public function json($response)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: applicaton/json; charset=UTF-8");
        http_response_code(200);
        echo json_encode($response);
        exit;
    }
}