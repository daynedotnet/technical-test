<?php

namespace Core;

/**
 * Class View
 * @package Core
 */
class View
{
    /**
     * @param $name
     */
    public function render($name)
    {
        $directory = explode('/', $name);
        $view = implode(DS, $directory);

        if (file_exists(ROOT . DS . 'app' . DS . 'views' . DS . $view . '.php')) {
            include(ROOT . DS . 'app' . DS . 'views' . DS . $view . '.php');
        } else {
            die('The view \"' . $view . '\" does not exist.');
        }
    }
}