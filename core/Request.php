<?php

namespace Core;

/**
 * Class Request
 * @package Core
 */
class Request
{
    /**
     * @return bool
     */
    public function isPost()
    {
        return $this->getRequestMethod() === 'POST';
    }

    /**
     * @return bool
     */
    public function isPut()
    {
        return $this->getRequestMethod() === 'PUT';
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return $this->getRequestMethod() === 'GET';
    }

    /**
     * @return string
     */
    public function getRequestMethod()
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }
}
