<?php

namespace Core;

use App\Factories\ControllerFactory;

/**
 * Class Router
 * @package Core
 */
class Router
{
    /**
     * @param $url
     */
    public static function route($url)
    {
        //controller
        $controller = (isset($url[0]) && $url[0] != '') ? ucwords($url[0]) . 'Controller' : DEFAULT_CONTROLLER;
        array_shift($url);

        //get method base on route
        $method = (isset($url[0]) && $url[0] != '') ? $url[0] : 'index';

        //params
        $controller = 'App\Controllers\\' . $controller;
        $dispatch = ControllerFactory::create($controller);

        if (method_exists($controller, $method)) {
            call_user_func([$dispatch, $method]);
        } else {
            die('That method does not exist in the controller \"' . $controller . '\"');
        }
    }
}
