<?php

use Core\Router;

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

// load configuration constants
require_once(ROOT . DS . 'config' . DS . 'config.php');

function autoload($name)
{
    $classes = explode('\\', $name);
    $class = array_pop($classes);
    $subPath = strtolower(implode(DS, $classes));
    $path = ROOT . DS . $subPath . DS . $class . '.php';
    if (file_exists($path)) {
        require_once($path);
    }
}

spl_autoload_register('autoload');

$url = isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'], '/')) : [];

// Route the request
Router::route($url);
