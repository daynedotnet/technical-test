$(document).ready(function () {
    $('.calculate-form').validate({
        rules: {
            studies: {
                required: true,
                digits: true,
                min: 1
            },
            growth: {
                required: true,
                digits: true,
                min: 0,
                max: 100
            },
            months: {
                required: true,
                digits: true,
                min: 1,
                max: 12
            }
        },
        messages: {
            studies: {
                required: 'Please enter current no. of study per day'
            },
            growth: {
                required: 'Please enter no. of study growth per month'
            },
            months: {
                required: 'Please enter no. of months to forecast'
            }
        },
        highlight: function (element) {
            $(element).addClass('invalid')
        },
        unhighlight: function (element) {
            $(element).removeClass('invalid')
        },
        submitHandler: function (form) {
            var frm = $(form),
                frmData = frm.serialize(),
                btn = frm.find('button');

            $.ajax({
                type: "POST",
                dataType: "json",
                url: btn.attr("data-url"),
                data: frmData,
                success: function (response) {
                    let table = $('.table-result'),
                        badge = $('.badge-result')

                    // empty table tbody rows
                    table.find('tbody > tr').empty();
                    // set result badge count
                    badge.text(response.length);

                    // add row for each result
                    $.each(response, function (key, value) {
                        table.find('tr:last').after("<tr>" +
                            "<td>" + value.month_year + "</td>" +
                            "<td>" + value.number_studies + "</td>" +
                            "<td>" + value.cost_forecasted + "</td>" +
                            "</tr>");
                    });
                },
                error: function (error) {
                    alert(error)
                }
            });
        }
    });
});